#ifndef CMDOPTIONLISTINT_HPP_
#define CMDOPTIONLISTINT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionListInt : public CmdOption
	{
		public:
					CmdOptionListInt(std::vector<std::string>);
					CmdOptionListInt(std::vector<std::string>, const char*);
					CmdOptionListInt(std::vector<std::string>, std::string);
					CmdOptionListInt(std::vector<std::string>, bool);
					CmdOptionListInt(std::vector<std::string>, const char*, bool);
					CmdOptionListInt(std::vector<std::string>, std::string, bool);
			std::string	setValue(const std::string& value);
			std::vector<int>		value() const;

		private:
			std::vector<int>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONLISTINT_HPP_ */
