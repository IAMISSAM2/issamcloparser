#ifndef CMDOPTIONREMAININGFILELISTFLOAT_HPP_
#define CMDOPTIONREMAININGFILELISTFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileListFloat : public CmdOption
	{
		public:
					CmdOptionRemainingFileListFloat(std::vector<std::string>);
					CmdOptionRemainingFileListFloat(std::vector<std::string>, const char*);
					CmdOptionRemainingFileListFloat(std::vector<std::string>, bool);
					CmdOptionRemainingFileListFloat(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::vector<float>>		value() const;

		private:
			std::vector<std::vector<float>>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONREMAININGFILELISTFLOAT_HPP_ */
