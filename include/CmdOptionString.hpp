#ifndef CMDOPTIONSTRING_HPP_
#define CMDOPTIONSTRING_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionString : public CmdOption
	{
		public:
					CmdOptionString(std::vector<std::string>);
					CmdOptionString(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::string		value() const;

		private:
			std::string		_value;
	};

#endif /* !CMDOPTIONSTRING_HPP_ */
