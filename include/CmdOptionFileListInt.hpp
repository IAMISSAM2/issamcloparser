#ifndef CMDOPTIONFILELISTINT_HPP_
#define CMDOPTIONFILELISTINT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileListInt : public CmdOption
	{
		public:
					CmdOptionFileListInt(std::vector<std::string>);
					CmdOptionFileListInt(std::vector<std::string>, const char*);
					CmdOptionFileListInt(std::vector<std::string>, bool);
					CmdOptionFileListInt(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<int>		value() const;

		private:
			std::vector<int>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONFILELISTINT_HPP_ */
