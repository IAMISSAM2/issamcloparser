#include "CmdOptionFloat.hpp"

CmdOptionFloat::CmdOptionFloat(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::Float;
	this->_value = 0;
	this->_enabled = false;
}

CmdOptionFloat::CmdOptionFloat(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::Float;
	this->_value = 0;
	this->_enabled = false;
}

std::string	CmdOptionFloat::setValue(const std::string& value)
{
	std::string error = "";
	try {
		this->_value = std::stof(value);
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

float		CmdOptionFloat::value() const
{
	return this->_value;
}