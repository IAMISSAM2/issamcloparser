#include "CmdOptionInt.hpp"

CmdOptionInt::CmdOptionInt(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::Int;
	this->_value = 0;
	this->_enabled = false;
}

CmdOptionInt::CmdOptionInt(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::Int;
	this->_value = 0;
	this->_enabled = false;
}

std::string	CmdOptionInt::setValue(const std::string& value)
{
	std::string error = "";
	try {
		this->_value = std::stoi(value);
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

int		CmdOptionInt::value() const
{
	return this->_value;
}