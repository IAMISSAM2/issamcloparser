#ifndef CMDOPTIONREMAININGFILELISTINT_HPP_
#define CMDOPTIONREMAININGFILELISTINT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileListInt : public CmdOption
	{
		public:
					CmdOptionRemainingFileListInt(std::vector<std::string>);
					CmdOptionRemainingFileListInt(std::vector<std::string>, const char*);
					CmdOptionRemainingFileListInt(std::vector<std::string>, bool);
					CmdOptionRemainingFileListInt(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::vector<int>>		value() const;

		private:
			std::vector<std::vector<int>>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONREMAININGFILELISTINT_HPP_ */
