#ifndef CMDOPTION_HPP_
#define CMDOPTION_HPP_

	#include <string>
	#include <vector>
	#include <iostream>
	
	class CmdOption
	{
		public:
			enum Type {
				None,
				String,
				Double,
				Float,
				Char,
				Int,
				FileString,
				FileFloat,
				FileDouble,
				FileChar,
				FileInt,
				ListString,
				ListDouble,
				ListFloat,
				ListChar,
				ListInt,
				FileListString,
				FileListDouble,
				FileListFloat,
				FileListChar,
				FileListInt,
				RemainingString,
				RemainingDouble,
				RemainingFloat,
				RemainingChar,
				RemainingInt,
				RemainingFileString,
				RemainingFileDouble,
				RemainingFileFloat,
				RemainingFileChar,
				RemainingFileInt,
				RemainingFileListString,
				RemainingFileListDouble,
				RemainingFileListFloat,
				RemainingFileListChar,
				RemainingFileListInt
			};

			std::vector<std::string>	getNames() const;
			void						enable();
			void						disable();
			bool						is_enabled() const;
			bool						is_optionnal() const;
			Type						type;
			
		protected:
			std::vector<std::string>	_names;
			bool						_optionnal;
			bool						_enabled;
	};

#endif /* !CMDOPTION_HPP_ */
