#ifndef CMDOPTIONREMAININGFILEINT_HPP_
#define CMDOPTIONREMAININGFILEINT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileInt : public CmdOption
	{
		public:
					CmdOptionRemainingFileInt(std::vector<std::string>);
					CmdOptionRemainingFileInt(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<int>		value() const;

		private:
			std::vector<int>		_value;
	};

#endif /* !CMDOPTIONREMAININGFILEINT_HPP_ */
