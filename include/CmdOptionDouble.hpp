#ifndef CMDOPTIONDOUBLE_HPP_
#define CMDOPTIONDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionDouble : public CmdOption
	{
		public:
					CmdOptionDouble(std::vector<std::string>);
					CmdOptionDouble(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			double		value() const;

		private:
			double		_value;
	};

#endif /* !CMDOPTIONDOUBLE_HPP_ */
