#include "CmdOptionRemainingFloat.hpp"

CmdOptionRemainingFloat::CmdOptionRemainingFloat(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFloat;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingFloat::CmdOptionRemainingFloat(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFloat;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingFloat::setValue(const std::string& value)
{
	std::string error = "";
	try {
		float tmp = std::stof(value);
		this->_value.push_back(tmp);
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

std::vector<float>		CmdOptionRemainingFloat::value() const
{
	return this->_value;
}