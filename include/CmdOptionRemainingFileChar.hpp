#ifndef CMDOPTIONREMAININGFILECHAR_HPP_
#define CMDOPTIONREMAININGFILECHAR_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileChar : public CmdOption
	{
		public:
					CmdOptionRemainingFileChar(std::vector<std::string>);
					CmdOptionRemainingFileChar(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<char>		value() const;

		private:
			std::vector<char>		_value;
	};

#endif /* !CMDOPTIONREMAININGFILECHAR_HPP_ */
