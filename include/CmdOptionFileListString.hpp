#ifndef CMDOPTIONFILELISTSTRING_HPP_
#define CMDOPTIONFILELISTSTRING_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileListString : public CmdOption
	{
		public:
					CmdOptionFileListString(std::vector<std::string>);
					CmdOptionFileListString(std::vector<std::string>, const char*);
					CmdOptionFileListString(std::vector<std::string>, bool);
					CmdOptionFileListString(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::string>		value() const;

		private:
			std::vector<std::string>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONFILELISTSTRING_HPP_ */
