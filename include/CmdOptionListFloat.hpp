#ifndef CMDOPTIONLISTFLOAT_HPP_
#define CMDOPTIONLISTFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionListFloat : public CmdOption
	{
		public:
					CmdOptionListFloat(std::vector<std::string>);
					CmdOptionListFloat(std::vector<std::string>, const char*);
					CmdOptionListFloat(std::vector<std::string>, std::string);
					CmdOptionListFloat(std::vector<std::string>, bool);
					CmdOptionListFloat(std::vector<std::string>, const char*, bool);
					CmdOptionListFloat(std::vector<std::string>, std::string, bool);
			std::string	setValue(const std::string& value);
			std::vector<float>		value() const;

		private:
			std::vector<float>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONLISTFLOAT_HPP_ */
