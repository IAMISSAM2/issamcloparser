#ifndef CMDOPTIONREMAININGINT_HPP_
#define CMDOPTIONREMAININGINT_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionRemainingInt : public CmdOption
	{
		public:
					CmdOptionRemainingInt(std::vector<std::string>);
					CmdOptionRemainingInt(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<int>		value() const;

		private:
			std::vector<int>		_value;
	};

#endif /* !CMDOPTIONREMAININGINT_HPP_ */
