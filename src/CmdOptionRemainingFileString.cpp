#include "CmdOptionRemainingFileString.hpp"

CmdOptionRemainingFileString::CmdOptionRemainingFileString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileString;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingFileString::CmdOptionRemainingFileString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileString;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingFileString::setValue(const std::string& value)
{
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	this->_value.push_back(str);

	return "";
}

std::vector<std::string>		CmdOptionRemainingFileString::value() const
{
	return this->_value;
}