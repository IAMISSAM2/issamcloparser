#include "CmdOptionListChar.hpp"

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names, std::string separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListChar::CmdOptionListChar(std::vector<std::string> names, std::string separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListFloat;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionListChar::setValue(const std::string& value)
{
	std::string error = "";
	std::string tmp = "";


	for (auto c : value) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				if (tmp.size() == 1) {
					this->_value.push_back(std::stof(tmp));
				} else {
					if (this->getNames().size() > 0) {
						error += "Valeur incorrecte pour l'option ";
						if (this->getNames().front().size() == 1) {
							error += "-";
						} else {
							error += "--";
						}
						error += this->getNames().front();
						error += ": « ";
						error += tmp;
						error += " »";
					} else {
						error += "valeur incorrecte: « ";
						error += tmp;
						error += " »";
					}
					return error;
				}
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		if (tmp.size() == 1) {
			this->_value.push_back(std::stof(tmp));
		} else {
			if (this->getNames().size() > 0) {
				error += "Valeur incorrecte pour l'option ";
				if (this->getNames().front().size() == 1) {
					error += "-";
				} else {
					error += "--";
				}
				error += this->getNames().front();
				error += ": « ";
				error += tmp;
				error += " »";
			} else {
				error += "valeur incorrecte: « ";
				error += tmp;
				error += " »";
			}
			return error;
		}
	}
	return error;
}

std::vector<char>		CmdOptionListChar::value() const
{
	return this->_value;
}