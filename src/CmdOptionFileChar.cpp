#include "CmdOptionFileChar.hpp"

CmdOptionFileChar::CmdOptionFileChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileChar;
	this->_value = 0;
	this->_enabled = false;
}

CmdOptionFileChar::CmdOptionFileChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileChar;
	this->_value = 0;
	this->_enabled = false;
}

std::string	CmdOptionFileChar::setValue(const std::string& value)
{
	std::string error;
	std::string tmp;
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	for (auto c : str) {
		if (c == '\n' || c == '\r') continue;
		tmp += c;
	}
	if (tmp.size() == 1) {
		this->_value = tmp.front();
	} else {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += tmp;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += tmp;
			error += " »";
		}
	}

	return error;
}

char		CmdOptionFileChar::value() const
{
	return this->_value;
}
