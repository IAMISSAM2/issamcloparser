#ifndef CMDOPTIONFILECHAR_HPP_
#define CMDOPTIONFILECHAR_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileChar : public CmdOption
	{
		public:
					CmdOptionFileChar(std::vector<std::string>);
					CmdOptionFileChar(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			char		value() const;

		private:
			char		_value;
	};

#endif /* !CMDOPTIONFILECHAR_HPP_ */
