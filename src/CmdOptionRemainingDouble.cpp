#include "CmdOptionRemainingDouble.hpp"

CmdOptionRemainingDouble::CmdOptionRemainingDouble(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingDouble;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingDouble::CmdOptionRemainingDouble(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingDouble;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingDouble::setValue(const std::string& value)
{
	std::string error = "";
	try {
		double tmp = std::stod(value);
		this->_value.push_back(tmp);
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

std::vector<double>		CmdOptionRemainingDouble::value() const
{
	return this->_value;
}