#include "CmdOptionRemainingFileChar.hpp"

CmdOptionRemainingFileChar::CmdOptionRemainingFileChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileChar;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingFileChar::CmdOptionRemainingFileChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileChar;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingFileChar::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	std::string tmp;
	for (auto c : str) {
		if (c == ' ' || c == '\n' || c == '\r' || c == '\t') { continue; }
		tmp += c;
	}

	if (tmp.size() == 1) {
		this->_value.push_back(tmp.front());
	} else {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		}
	}

	return error;
}

std::vector<char>		CmdOptionRemainingFileChar::value() const
{
	return this->_value;
}