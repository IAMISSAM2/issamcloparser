#ifndef CMDOPTIONLISTSTRING_HPP_
#define CMDOPTIONLISTSTRING_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionListString : public CmdOption
	{
		public:
					CmdOptionListString(std::vector<std::string>);
					CmdOptionListString(std::vector<std::string>, const char*);
					CmdOptionListString(std::vector<std::string>, std::string);
					CmdOptionListString(std::vector<std::string>, bool);
					CmdOptionListString(std::vector<std::string>, const char*, bool);
					CmdOptionListString(std::vector<std::string>, std::string, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::string>		value() const;

		private:
			std::vector<std::string>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONLISTSTRING_HPP_ */
