#include "IssamCloParser.hpp"

bool		match_command_name(const std::string& with_prefix, const std::string& without_prefix)
{
	if (without_prefix.size() == 1) {
		return with_prefix.front() == '-' && with_prefix.substr(1) == without_prefix;
	} else {
		return with_prefix.rfind("--", 0) == 0 && with_prefix.substr(2) == without_prefix;
	}
	return false;
}

bool		command_exists(const std::string& with_prefix, std::vector<CmdOption*> options)
{
	if (with_prefix.size() == 1) {
		return true;
	}
	for (auto option : options) {
		for (auto name : option->getNames()) {
			if (name.size() == 1) {
				if (with_prefix.substr(1) == name) {
					return true;
				}
			} else {
				if (with_prefix.substr(2) == name) {
					return true;
				}
			}
		}
	}
	return false;
}

int		IssamCloParser::set_value(CmdOption* option, const std::string& value)
{
	std::string err = "";

	switch (option->type) {
		case CmdOption::Type::Int:
			err = ((CmdOptionInt*)option)->setValue(value);
			break;
		case CmdOption::Type::String:
			err = ((CmdOptionString*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingInt:
			err = ((CmdOptionRemainingInt*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingString:
			err = ((CmdOptionRemainingString*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileInt:
			err = ((CmdOptionRemainingFileInt*)option)->setValue(value);
			break;
		case CmdOption::Type::FileListInt:
			err = ((CmdOptionFileListInt*)option)->setValue(value);
			break;
		case CmdOption::Type::Float:
			err = ((CmdOptionFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::Double:
			err = ((CmdOptionDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::Char:
			err = ((CmdOptionChar*)option)->setValue(value);
			break;
		case CmdOption::Type::FileInt:
			err = ((CmdOptionFileInt*)option)->setValue(value);
			break;
		case CmdOption::Type::ListInt:
			err = ((CmdOptionListInt*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileListInt:
			err = ((CmdOptionRemainingFileListInt*)option)->setValue(value);
			break;
		case CmdOption::Type::FileFloat:
			err = ((CmdOptionFileFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::FileString:
			err = ((CmdOptionFileString*)option)->setValue(value);
			break;
		case CmdOption::Type::FileChar:
			err = ((CmdOptionFileChar*)option)->setValue(value);
			break;
		case CmdOption::Type::FileDouble:
			err = ((CmdOptionFileDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::ListFloat:
			err = ((CmdOptionListFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::ListString:
			err = ((CmdOptionListString*)option)->setValue(value);
			break;
		case CmdOption::Type::ListChar:
			err = ((CmdOptionListChar*)option)->setValue(value);
			break;
		case CmdOption::Type::ListDouble:
			err = ((CmdOptionListDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFloat:
			err = ((CmdOptionRemainingFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingChar:
			err = ((CmdOptionRemainingChar*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingDouble:
			err = ((CmdOptionRemainingDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::FileListFloat:
			err = ((CmdOptionFileListFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::FileListString:
			err = ((CmdOptionFileListString*)option)->setValue(value);
			break;
		case CmdOption::Type::FileListChar:
			err = ((CmdOptionFileListChar*)option)->setValue(value);
			break;
		case CmdOption::Type::FileListDouble:
			err = ((CmdOptionFileListDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileFloat:
			err = ((CmdOptionRemainingFileFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileString:
			err = ((CmdOptionRemainingFileString*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileChar:
			err = ((CmdOptionRemainingFileChar*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileDouble:
			err = ((CmdOptionRemainingFileListDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileListDouble:
			err = ((CmdOptionRemainingFileListDouble*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileListString:
			err = ((CmdOptionRemainingFileListString*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileListFloat:
			err = ((CmdOptionRemainingFileListFloat*)option)->setValue(value);
			break;
		case CmdOption::Type::RemainingFileListChar:
			err = ((CmdOptionRemainingFileListChar*)option)->setValue(value);
			break;
		default:
			break;
	}
	if (err.size()) {
		this->_error = true;
		this->_error_message = this->_binname;
		this->_error_message += ": ";
		this->_error_message += err;
	}
	return err.size();
}

void		IssamCloParser::parse(std::vector<CmdOption*> options)
{
	bool end_command = false;

	//Parse commands
	for (auto option : options) {
		if (option->getNames().size() > 0) {
			for (auto name : option->getNames()) {
				for (auto it = this->_parsed_arguments.begin(); it < this->_parsed_arguments.end(); it++) {
					if (*it == "--") {
						break;
					}
					if (match_command_name(*it, name)) {
						option->enable();
						if (option->type == CmdOption::Type::None) {
							it = this->_parsed_arguments.erase(it)-1;
						} else {
							if (set_value(option, it + 1 < this->_parsed_arguments.end() ? *(it + 1) : "")) {
								return;
							}
							it = this->_parsed_arguments.erase(it);
							if (it < this->_parsed_arguments.end()) {
								it = this->_parsed_arguments.erase(it);
							}
						}
					}
				}
			}
			if (!option->is_enabled() && !option->is_optionnal()) {
				this->_error = true;
				this->_error_message += this->_binname;
				this->_error_message += " : option '-";
				if (option->getNames().front().size() > 1) {
					this->_error_message += '-';
				}
				this->_error_message += option->getNames().front();
				this->_error_message += "' manquante.";
				if (this->_default_help) {
					this->_error_message += "\n\nSaisissez « ";
					this->_error_message += _binname;
					this->_error_message += " --help » pour plus d'informations.";
				}
				return;
			}
		}
	}

	//Parse bad command
	for (auto it = this->_parsed_arguments.begin(); it < this->_parsed_arguments.end(); it++) {
		if (*it == "--") {
			break;
		}
		if (!(*it).rfind("-", 0) && (*it).size() > 1 && std::isalpha((*it)[1+(*it).rfind("-", 1)]) && !command_exists(*it, options)) {
			this->_error = true;
			this->_error_message = this->_binname + ": option invalide « " + *it + " ».";
			return;
		}
	}

	//Parse positional parameters
	for (auto option : options) {
		if (option->getNames().size() == 0 && option->type < CmdOption::Type::RemainingString) {
			end_command = false;
			for (auto it = this->_parsed_arguments.begin(); it < this->_parsed_arguments.end(); it++) {
				if (*it == "--" && end_command == false) {
					end_command = true;
					continue;
				}
				option->enable();
				if (set_value(option, *it)) {
					return;
				}
				it = this->_parsed_arguments.erase(it);
				break;
			}
			if (!option->is_enabled()) {
				this->_match = false;
			}
		}
	}
	// Parse Remaining Parameters
	for (auto option : options) {
		if (option->type >= CmdOption::Type::RemainingString) {
			end_command = false;
			for (auto it = this->_parsed_arguments.begin(); it < this->_parsed_arguments.end(); it++) {
				if (!end_command && *it == "--") {
					end_command = true;
					continue;
				}
				if (set_value(option, *it)) {
					return;
				}
			}
		}
	}
	if (this->_parsed_arguments.size() > 1 || (this->_parsed_arguments.size() == 1 && this->_parsed_arguments.front() != "--")) {
		this->_match = false;
	}

}

bool	command_takes_argument(char command, std::vector<CmdOption*> options)
{
	for (auto option : options) 
	{
		if (option->type != CmdOption::Type::None) {
			for (auto name : option->getNames()) {
				if (name.size() == 1 && name.front() == command) {
					return true;
				}
			}
		}
	}
	return false;
}

void	self_command_conflict(CmdOption* option)
{
	std::vector<std::string> names = option->getNames();

	if (names.size()) {
		for (size_t i = 0; i < names.size() - 1; i += 1) {
			for (size_t j = i + 1; j < names.size(); j += 1) {
				if (names[i] == names[j]) {
					std::cerr << "Commande dupliquée : " << (names[i].size() > 1 ? "--" : "-") << names[i] << std::endl;
					exit(1);
				}
			}
		}
	}
}

void	check_duplicated_commands(std::vector<CmdOption*> options)
{
	if (options.size()) {
		if (options.size() == 1) {
			self_command_conflict(options[0]);
		}
		for (size_t i = 0; i < options.size() - 1; i += 1) {
			self_command_conflict(options[i]);
			for (size_t j = i + 1; j < options.size(); j += 1) {
				std::vector<std::string> names1 = options[i]->getNames();
				std::vector<std::string> names2 = options[j]->getNames();
				for (auto name1 : names1) {
					for (auto name2 : names2) {
						if (name1 == name2) {
							std::cerr << "Commande dupliquée : " << (name1.size() > 1 ? "--" : "-") << name1 << std::endl;
							exit(1);
						}
					}
				}
			}
		}
	}
}

void	check_multiple_remaining_options(std::vector<CmdOption*> options)
{
	int remaining_option_count = 0;

	for (auto option : options) {
		remaining_option_count += option->type >= CmdOption::Type::RemainingString;
	}
	if (remaining_option_count > 1) {
		std::cerr << "Trop d'option CmdOptionRemainingXXX (1 maximum)\n";
		exit(1);
	}
}

IssamCloParser::IssamCloParser(std::vector<CmdOption*> options, int ac, char** av)
{
	this->_match = true;
	this->_error = false;
	this->_binname = std::string(av[0]);
	this->_error_message = "";
	this->_default_help = false;
	this->_parsed_arguments.clear();

	for (auto option : options) {
		option->disable();
	}
	check_duplicated_commands(options);
	check_multiple_remaining_options(options);

	for (int i = 1; i < ac; i += 1) {
		std::string tmp(av[i]);
		auto equal_sign = std::find(tmp.begin(), tmp.end(), '=');

		if (equal_sign != tmp.end() && tmp.size() > 1 && !tmp.rfind("--", 0)) {
			this->_parsed_arguments.push_back(tmp.substr(0, equal_sign - tmp.begin()));
			this->_parsed_arguments.push_back(tmp.substr(equal_sign - tmp.begin() + 1));
		} else if (tmp.size() > 1 && tmp.rfind("--", 0) && !tmp.rfind("-", 0) && std::isalpha(tmp[1])) {
			size_t j = 1;
			for (;j < tmp.size() && std::isalpha(tmp[j]); j += 1) {
				std::string shortcmd = "-";
				shortcmd += tmp[j];
				this->_parsed_arguments.push_back(shortcmd);
				if (command_takes_argument(tmp[j], options)) {
					j += 1;
					break;
				}
			}
			if (j < tmp.size()) {
				this->_parsed_arguments.push_back(tmp.substr(j));
			}
		}
		else {
			this->_parsed_arguments.push_back(tmp);
		}
	}

	this->parse(options);
}

IssamCloParser::IssamCloParser(std::vector<CmdOption*> options, int ac, char** av, bool default_help)
{
	this->_match = true;
	this->_error = false;
	this->_binname = std::string(av[0]);
	this->_error_message = "";
	this->_default_help = default_help;
	this->_parsed_arguments.clear();
	for (int i = 1; i < ac; i += 1) {
		std::string tmp(av[i]);
		auto equal_sign = std::find(tmp.begin(), tmp.end(), '=');

		if (equal_sign != tmp.end() && tmp.size() > 1 && !tmp.rfind("--", 0)) {
			this->_parsed_arguments.push_back(tmp.substr(0, equal_sign - tmp.begin()));
			this->_parsed_arguments.push_back(tmp.substr(equal_sign - tmp.begin() + 1));
		} else if (tmp.rfind("--", 0) && !tmp.rfind("-", 0)) {
			size_t i = 1 ;
			for (;i < tmp.size() && std::isalpha(tmp[i]); i += 1) {
				std::string shortcmd = "-";
				shortcmd += tmp[i];
				this->_parsed_arguments.push_back(shortcmd);
				if (command_takes_argument(tmp[i], options)) {
					i += 1;
					break;
				}
			}
			if (i < tmp.size()) {
				this->_parsed_arguments.push_back(tmp.substr(i));
			}
		}
		else {
			this->_parsed_arguments.push_back(tmp);
		}
	}
	this->parse(options);
}

IssamCloParser::~IssamCloParser()
{
}

bool		IssamCloParser::error() const
{
	return this->_error;
}

std::string	IssamCloParser::what() const
{
	return this->_error_message;
}

bool		IssamCloParser::match() const
{
	return this->_match;
}