NAME	= libissamcloparser.a

CC	= g++

RM	= rm -f

SRCS	=	./src/CmdOptionRemainingFileListString.cpp \
			./src/CmdOptionRemainingFileListDouble.cpp \
			./src/CmdOptionRemainingFileListFloat.cpp \
			./src/CmdOptionRemainingFileListChar.cpp \
			./src/CmdOptionRemainingFileListInt.cpp \
			./src/CmdOptionRemainingFileString.cpp \
			./src/CmdOptionRemainingFileDouble.cpp \
			./src/CmdOptionRemainingFileFloat.cpp \
			./src/CmdOptionRemainingFileChar.cpp \
			./src/CmdOptionRemainingFileInt.cpp \
			./src/CmdOptionRemainingString.cpp \
			./src/CmdOptionRemainingDouble.cpp \
			./src/CmdOptionRemainingFloat.cpp \
			./src/CmdOptionFileListString.cpp \
			./src/CmdOptionFileListDouble.cpp \
			./src/CmdOptionFileListFloat.cpp \
			./src/CmdOptionRemainingChar.cpp \
			./src/CmdOptionFileListChar.cpp \
			./src/CmdOptionRemainingInt.cpp \
			./src/CmdOptionFileListInt.cpp \
			./src/CmdOptionFileString.cpp \
			./src/CmdOptionFileDouble.cpp \
			./src/CmdOptionListString.cpp \
			./src/CmdOptionListDouble.cpp \
			./src/CmdOptionListFloat.cpp \
			./src/CmdOptionFileFloat.cpp \
			./src/CmdOptionListChar.cpp \
			./src/CmdOptionFileChar.cpp \
			./src/CmdOptionFileInt.cpp \
			./src/CmdOptionListInt.cpp \
			./src/CmdOptionDouble.cpp \
			./src/CmdOptionString.cpp \
			./src/CmdOptionFloat.cpp \
			./src/IssamCloParser.cpp \
			./src/CmdOptionNone.cpp \
			./src/CmdOptionChar.cpp \
			./src/CmdOptionInt.cpp \
			./src/CmdOption.cpp \

OBJS	= $(SRCS:.cpp=.o)

CPPFLAGS = -Iinclude/
CPPFLAGS += -Wall -Wextra

all: $(NAME)

$(NAME): $(OBJS)
	@ar rc $(NAME) $(OBJS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
