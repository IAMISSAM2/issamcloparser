#ifndef CMDOPTIONREMAININGFILELISTCHAR_HPP_
#define CMDOPTIONREMAININGFILELISTCHAR_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileListChar : public CmdOption
	{
		public:
					CmdOptionRemainingFileListChar(std::vector<std::string>);
					CmdOptionRemainingFileListChar(std::vector<std::string>, const char*);
					CmdOptionRemainingFileListChar(std::vector<std::string>, bool);
					CmdOptionRemainingFileListChar(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::vector<char>>		value() const;

		private:
			std::vector<std::vector<char>>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONREMAININGFILELISTCHAR_HPP_ */
