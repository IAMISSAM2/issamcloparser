#include "CmdOptionRemainingFileListChar.hpp"

CmdOptionRemainingFileListChar::CmdOptionRemainingFileListChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileListChar;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionRemainingFileListChar::CmdOptionRemainingFileListChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileListChar;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionRemainingFileListChar::CmdOptionRemainingFileListChar(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileListChar;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionRemainingFileListChar::CmdOptionRemainingFileListChar(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileListChar;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionRemainingFileListChar::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());
	std::string tmp = "";
	std::vector<char> tmp2;


	for (auto c : str) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				if (tmp.size() == 1) {
					tmp2.push_back(tmp.front());
				} else {
					if (this->getNames().size() > 0) {
						error += "Valeur incorrecte pour l'option ";
						if (this->getNames().front().size() == 1) {
							error += "-";
						} else {
							error += "--";
						}
						error += this->getNames().front();
						error += ": « ";
						error += tmp;
						error += " »";
					} else {
						error += "valeur incorrecte: « ";
						error += tmp;
						error += " »";
					}
					return error;
				}
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		if (tmp.size() == 1) {
			tmp2.push_back(tmp.front());
		} else {
			if (this->getNames().size() > 0) {
				error += "Valeur incorrecte pour l'option ";
				if (this->getNames().front().size() == 1) {
					error += "-";
				} else {
					error += "--";
				}
				error += this->getNames().front();
				error += ": « ";
				error += tmp;
				error += " »";
			} else {
				error += "valeur incorrecte: « ";
				error += tmp;
				error += " »";
			}
			return error;
		}
	}

	this->_value.push_back(tmp2);
	return error;
}

std::vector<std::vector<char>>		CmdOptionRemainingFileListChar::value() const
{
	return this->_value;
}