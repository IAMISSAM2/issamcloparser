#ifndef CMDOPTIONFILEDOUBLE_HPP_
#define CMDOPTIONFILEDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileDouble : public CmdOption
	{
		public:
					CmdOptionFileDouble(std::vector<std::string>);
					CmdOptionFileDouble(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			double		value() const;

		private:
			double		_value;
	};

#endif /* !CMDOPTIONFILEDOUBLE_HPP_ */
