#include "CmdOptionRemainingFileListString.hpp"

CmdOptionRemainingFileListString::CmdOptionRemainingFileListString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionRemainingFileListString::CmdOptionRemainingFileListString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionRemainingFileListString::CmdOptionRemainingFileListString(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionRemainingFileListString::CmdOptionRemainingFileListString(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionRemainingFileListString::setValue(const std::string& value)
{
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());
	std::string tmp = "";
	std::vector<std::string> tmp2;


	for (auto c : str) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				tmp2.push_back(tmp);
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		tmp2.push_back(tmp);
	}

	this->_value.push_back(tmp2);
	return "";
}

std::vector<std::vector<std::string>>		CmdOptionRemainingFileListString::value() const
{
	return this->_value;
}