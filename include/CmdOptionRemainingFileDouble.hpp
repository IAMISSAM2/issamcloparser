#ifndef CMDOPTIONREMAININGFILEDOUBLE_HPP_
#define CMDOPTIONREMAININGFILEDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileDouble : public CmdOption
	{
		public:
					CmdOptionRemainingFileDouble(std::vector<std::string>);
					CmdOptionRemainingFileDouble(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<double>		value() const;

		private:
			std::vector<double>		_value;
	};

#endif /* !CMDOPTIONREMAININGFILEDOUBLE_HPP_ */
