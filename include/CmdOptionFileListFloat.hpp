#ifndef CMDOPTIONFILELISTFLOAT_HPP_
#define CMDOPTIONFILELISTFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileListFloat : public CmdOption
	{
		public:
					CmdOptionFileListFloat(std::vector<std::string>);
					CmdOptionFileListFloat(std::vector<std::string>, const char*);
					CmdOptionFileListFloat(std::vector<std::string>, bool);
					CmdOptionFileListFloat(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<float>		value() const;

		private:
			std::vector<float>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONFILELISTFLOAT_HPP_ */
