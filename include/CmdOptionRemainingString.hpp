#ifndef CMDOPTIONREMAININGSTRING_HPP_
#define CMDOPTIONREMAININGSTRING_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionRemainingString : public CmdOption
	{
		public:
					CmdOptionRemainingString(std::vector<std::string>);
					CmdOptionRemainingString(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::string>		value() const;

		private:
			std::vector<std::string>		_value;
	};

#endif /* !CMDOPTIONREMAININGSTRING_HPP_ */
