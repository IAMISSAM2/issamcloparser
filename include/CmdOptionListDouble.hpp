#ifndef CMDOPTIONLISTDOUBLE_HPP_
#define CMDOPTIONLISTDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionListDouble : public CmdOption
	{
		public:
					CmdOptionListDouble(std::vector<std::string>);
					CmdOptionListDouble(std::vector<std::string>, const char*);
					CmdOptionListDouble(std::vector<std::string>, std::string);
					CmdOptionListDouble(std::vector<std::string>, bool);
					CmdOptionListDouble(std::vector<std::string>, const char*, bool);
					CmdOptionListDouble(std::vector<std::string>, std::string, bool);
			std::string	setValue(const std::string& value);
			std::vector<double>		value() const;

		private:
			std::vector<double>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONLISTDOUBLE_HPP_ */
