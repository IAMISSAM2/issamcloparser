#ifndef ISSAMCLOPARSER_HPP_
#define ISSAMCLOPARSER_HPP_

	#include "CmdOptionRemainingFileListString.hpp"
	#include "CmdOptionRemainingFileListDouble.hpp"
	#include "CmdOptionRemainingFileListFloat.hpp"
	#include "CmdOptionRemainingFileListChar.hpp"
	#include "CmdOptionRemainingFileListInt.hpp"
	#include "CmdOptionRemainingFileString.hpp"
	#include "CmdOptionRemainingFileDouble.hpp"
	#include "CmdOptionRemainingFileFloat.hpp"
	#include "CmdOptionRemainingFileChar.hpp"
	#include "CmdOptionRemainingFileInt.hpp"
	#include "CmdOptionRemainingString.hpp"
	#include "CmdOptionRemainingDouble.hpp"
	#include "CmdOptionRemainingFloat.hpp"
	#include "CmdOptionFileListString.hpp"
	#include "CmdOptionFileListDouble.hpp"
	#include "CmdOptionFileListFloat.hpp"
	#include "CmdOptionRemainingChar.hpp"
	#include "CmdOptionFileListChar.hpp"
	#include "CmdOptionRemainingInt.hpp"
	#include "CmdOptionFileListInt.hpp"
	#include "CmdOptionFileString.hpp"
	#include "CmdOptionFileDouble.hpp"
	#include "CmdOptionListDouble.hpp"
	#include "CmdOptionListString.hpp"
	#include "CmdOptionListFloat.hpp"
	#include "CmdOptionFileFloat.hpp"
	#include "CmdOptionListChar.hpp"
	#include "CmdOptionFileChar.hpp"
	#include "CmdOptionFileInt.hpp"
	#include "CmdOptionListInt.hpp"
	#include "CmdOptionString.hpp"
	#include "CmdOptionDouble.hpp"
	#include "CmdOptionFloat.hpp"
	#include "CmdOptionNone.hpp"
	#include "CmdOptionChar.hpp"
	#include "CmdOptionInt.hpp"
	#include <algorithm>
	#include <vector>
	#include <string>

	class CmdOption;

	typedef std::vector<std::string> OptNames;
	typedef std::vector<CmdOption*> OptionList;

	class IssamCloParser {
		public:
			IssamCloParser(std::vector<CmdOption*>, int arg_count, char** arg_values);
			IssamCloParser(std::vector<CmdOption*>, int arg_count, char** arg_values, bool default_help);
			~IssamCloParser();
			bool		error() const;
			std::string	what() const;
			bool		match() const;
			void		setDefaultHelp(bool);

		private:
			int								set_value(CmdOption*, const std::string&);
			void							parse(std::vector<CmdOption*>);
			std::vector<std::string>		_parsed_arguments;
			bool							_match;
			bool							_error;
			std::string						_binname;
			std::string						_error_message;
			bool							_default_help;
	};

#endif /* !ISSAMCLOPARSER_HPP_ */
