#ifndef CMDOPTIONLISTCHAR_HPP_
#define CMDOPTIONLISTCHAR_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionListChar : public CmdOption
	{
		public:
					CmdOptionListChar(std::vector<std::string>);
					CmdOptionListChar(std::vector<std::string>, const char*);
					CmdOptionListChar(std::vector<std::string>, std::string);
					CmdOptionListChar(std::vector<std::string>, bool);
					CmdOptionListChar(std::vector<std::string>, const char*, bool);
					CmdOptionListChar(std::vector<std::string>, std::string, bool);
			std::string	setValue(const std::string& value);
			std::vector<char>		value() const;

		private:
			std::vector<char>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONLISTCHAR_HPP_ */
