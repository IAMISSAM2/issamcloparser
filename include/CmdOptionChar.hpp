#ifndef CMDOPTIONCHAR_HPP_
#define CMDOPTIONCHAR_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionChar : public CmdOption
	{
		public:
					CmdOptionChar(std::vector<std::string>);
					CmdOptionChar(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			char		value() const;

		private:
			char		_value;
	};

#endif /* !CMDOPTIONCHAR_HPP_ */
