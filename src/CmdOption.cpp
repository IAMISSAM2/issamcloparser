#include "CmdOption.hpp"

std::vector<std::string>	CmdOption::getNames() const
{
	return this->_names;
}

void						CmdOption::enable()
{
	this->_enabled = true;
}

void						CmdOption::disable()
{
	this->_enabled = false;
}

bool						CmdOption::is_enabled() const
{
	return this->_enabled;
}

bool						CmdOption::is_optionnal() const
{
	return this->_optionnal;
}