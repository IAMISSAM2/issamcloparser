#ifndef CMDOPTIONREMAININGFILELISTDOUBLE_HPP_
#define CMDOPTIONREMAININGFILELISTDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileListDouble : public CmdOption
	{
		public:
					CmdOptionRemainingFileListDouble(std::vector<std::string>);
					CmdOptionRemainingFileListDouble(std::vector<std::string>, const char*);
					CmdOptionRemainingFileListDouble(std::vector<std::string>, bool);
					CmdOptionRemainingFileListDouble(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::vector<double>>		value() const;

		private:
			std::vector<std::vector<double>>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONREMAININGFILELISTDOUBLE_HPP_ */
