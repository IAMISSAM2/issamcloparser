#ifndef CMDOPTIONREMAININGDOUBLE_HPP_
#define CMDOPTIONREMAININGDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionRemainingDouble : public CmdOption
	{
		public:
					CmdOptionRemainingDouble(std::vector<std::string>);
					CmdOptionRemainingDouble(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<double>		value() const;

		private:
			std::vector<double>		_value;
	};

#endif /* !CMDOPTIONREMAININGDOUBLE_HPP_ */
