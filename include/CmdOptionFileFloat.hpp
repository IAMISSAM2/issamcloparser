#ifndef CMDOPTIONFILEFLOAT_HPP_
#define CMDOPTIONFILEFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileFloat : public CmdOption
	{
		public:
					CmdOptionFileFloat(std::vector<std::string>);
					CmdOptionFileFloat(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			float		value() const;

		private:
			float		_value;
	};

#endif /* !CMDOPTIONFILEFLOAT_HPP_ */
