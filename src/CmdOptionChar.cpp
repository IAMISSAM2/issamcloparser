#include "CmdOptionChar.hpp"

CmdOptionChar::CmdOptionChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::Char;
	this->_value = 0;
	this->_enabled = false;
}

CmdOptionChar::CmdOptionChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::Char;
	this->_value = 0;
	this->_enabled = false;
}

std::string	CmdOptionChar::setValue(const std::string& value)
{
	std::string error = "";
	if (value.size() == 1) {
		this->_value = value.front();
	} else {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

char		CmdOptionChar::value() const
{
	return this->_value;
}