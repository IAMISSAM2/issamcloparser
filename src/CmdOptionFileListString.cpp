#include "CmdOptionFileListString.hpp"

CmdOptionFileListString::CmdOptionFileListString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionFileListString::CmdOptionFileListString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionFileListString::CmdOptionFileListString(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionFileListString::CmdOptionFileListString(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionFileListString::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());
	std::string tmp = "";


	for (auto c : str) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				this->_value.push_back(tmp);
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		this->_value.push_back(tmp);
	}
	return error;
}

std::vector<std::string>		CmdOptionFileListString::value() const
{
	return this->_value;
}