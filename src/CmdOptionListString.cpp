#include "CmdOptionListString.hpp"

CmdOptionListString::CmdOptionListString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionListString::CmdOptionListString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionListString::CmdOptionListString(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListString::CmdOptionListString(std::vector<std::string> names, std::string separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListString::CmdOptionListString(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionListString::CmdOptionListString(std::vector<std::string> names, std::string separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::ListString;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionListString::setValue(const std::string& value)
{
	std::string tmp = "";

	for (auto c : value) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				this->_value.push_back(tmp);
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		this->_value.push_back(tmp);
	}
	return "";
}

std::vector<std::string>		CmdOptionListString::value() const
{
	return this->_value;
}