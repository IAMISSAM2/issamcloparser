#ifndef CMDOPTIONFILEINT_HPP_
#define CMDOPTIONFILEINT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileInt : public CmdOption
	{
		public:
					CmdOptionFileInt(std::vector<std::string>);
					CmdOptionFileInt(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			int		value() const;

		private:
			int		_value;
	};

#endif /* !CMDOPTIONFILEINT_HPP_ */
