#include "CmdOptionString.hpp"

CmdOptionString::CmdOptionString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::String;
	this->_value = "";
	this->_enabled = false;
}

CmdOptionString::CmdOptionString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::String;
	this->_value = "";
	this->_enabled = false;
}

std::string	CmdOptionString::setValue(const std::string& value)
{
	this->_value = std::string(value);
	return "";
}

std::string		CmdOptionString::value() const
{
	return this->_value;
}