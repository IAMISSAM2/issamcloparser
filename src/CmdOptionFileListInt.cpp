#include "CmdOptionFileListInt.hpp"

CmdOptionFileListInt::CmdOptionFileListInt(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileListInt;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionFileListInt::CmdOptionFileListInt(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileListInt;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = " \t\n";
}

CmdOptionFileListInt::CmdOptionFileListInt(std::vector<std::string> names, const char* separator)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileListInt;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

CmdOptionFileListInt::CmdOptionFileListInt(std::vector<std::string> names, const char* separator, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileListInt;
	this->_value.clear();
	this->_enabled = false;
	this->_separator = separator;
}

std::string	CmdOptionFileListInt::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());
	std::string tmp = "";


	for (auto c : str) {
		if (this->_separator.find(c) != std::string::npos) {
			if (tmp.size() > 0) {
				try {
					this->_value.push_back(std::stoi(tmp));
				} catch (std::invalid_argument&) {
					if (this->getNames().size() > 0) {
						error += "Valeur incorrecte pour l'option ";
						if (this->getNames().front().size() == 1) {
							error += "-";
						} else {
							error += "--";
						}
						error += this->getNames().front();
						error += ": « ";
						error += tmp;
						error += " »";
					} else {
						error += "valeur incorrecte: « ";
						error += tmp;
						error += " »";
					}
					return error;
				}
			}
			tmp.clear();
		} else {
			tmp += c;
		}
	}
	if (tmp.size() > 0) {
		try {
			this->_value.push_back(std::stoi(tmp));
		} catch (std::invalid_argument&) {
			if (this->getNames().size() > 0) {
				error += "Valeur incorrecte pour l'option ";
				if (this->getNames().front().size() == 1) {
					error += "-";
				} else {
					error += "--";
				}
				error += this->getNames().front();
				error += ": « ";
				error += tmp;
				error += " »";
			} else {
				error += "valeur incorrecte: « ";
				error += tmp;
				error += " »";
			}
			return error;
		}
	}
	return error;
}

std::vector<int>		CmdOptionFileListInt::value() const
{
	return this->_value;
}