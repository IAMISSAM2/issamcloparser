#ifndef CMDOPTIONINT_HPP_
#define CMDOPTIONINT_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionInt : public CmdOption
	{
		public:
					CmdOptionInt(std::vector<std::string>);
					CmdOptionInt(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			int		value() const;

		private:
			int		_value;
	};

#endif /* !CMDOPTIONINT_HPP_ */
