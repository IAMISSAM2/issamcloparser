#ifndef CMDOPTIONREMAININGFILEFLOAT_HPP_
#define CMDOPTIONREMAININGFILEFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileFloat : public CmdOption
	{
		public:
					CmdOptionRemainingFileFloat(std::vector<std::string>);
					CmdOptionRemainingFileFloat(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<float>		value() const;

		private:
			std::vector<float>		_value;
	};

#endif /* !CMDOPTIONREMAININGFILEFLOAT_HPP_ */
