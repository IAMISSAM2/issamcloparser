#ifndef CMDOPTIONFLOAT_HPP_
#define CMDOPTIONFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionFloat : public CmdOption
	{
		public:
					CmdOptionFloat(std::vector<std::string>);
					CmdOptionFloat(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			float		value() const;

		private:
			float		_value;
	};

#endif /* !CMDOPTIONFLOAT_HPP_ */
