#ifndef CMDOPTIONFILESTRING_HPP_
#define CMDOPTIONFILESTRING_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileString : public CmdOption
	{
		public:
					CmdOptionFileString(std::vector<std::string>);
					CmdOptionFileString(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::string		value() const;

		private:
			std::string		_value;
	};

#endif /* !CMDOPTIONFILESTRING_HPP_ */
