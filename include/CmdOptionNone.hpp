#ifndef CMDOPTIONNONE_HPP_
#define CMDOPTIONNONE_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionNone : public CmdOption
	{
		public:
					CmdOptionNone(std::vector<std::string>);
					CmdOptionNone(std::vector<std::string>, bool);

	};

#endif /* !CMDOPTIONNONE_HPP_ */
