#ifndef CMDOPTIONREMAININGFILELISTSTRING_HPP_
#define CMDOPTIONREMAININGFILELISTSTRING_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileListString : public CmdOption
	{
		public:
					CmdOptionRemainingFileListString(std::vector<std::string>);
					CmdOptionRemainingFileListString(std::vector<std::string>, const char*);
					CmdOptionRemainingFileListString(std::vector<std::string>, bool);
					CmdOptionRemainingFileListString(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::vector<std::string>>		value() const;

		private:
			std::vector<std::vector<std::string>>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONREMAININGFILELISTSTRING_HPP_ */
