#ifndef CMDOPTIONFILELISTCHAR_HPP_
#define CMDOPTIONFILELISTCHAR_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileListChar : public CmdOption
	{
		public:
					CmdOptionFileListChar(std::vector<std::string>);
					CmdOptionFileListChar(std::vector<std::string>, const char*);
					CmdOptionFileListChar(std::vector<std::string>, bool);
					CmdOptionFileListChar(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<char>		value() const;

		private:
			std::vector<char>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONFILELISTCHAR_HPP_ */
