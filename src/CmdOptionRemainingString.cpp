#include "CmdOptionRemainingString.hpp"

CmdOptionRemainingString::CmdOptionRemainingString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingString;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingString::CmdOptionRemainingString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingString;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingString::setValue(const std::string& value)
{
	this->_value.push_back(value);
	return "";
}

std::vector<std::string>		CmdOptionRemainingString::value() const
{
	return this->_value;
}