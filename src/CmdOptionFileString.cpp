#include "CmdOptionFileString.hpp"

CmdOptionFileString::CmdOptionFileString(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileString;
	this->_value = "";
	this->_enabled = false;
}

CmdOptionFileString::CmdOptionFileString(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileString;
	this->_value = "";
	this->_enabled = false;
}

std::string	CmdOptionFileString::setValue(const std::string& value)
{
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	this->_value = str;

	return "";
}

std::string		CmdOptionFileString::value() const
{
	return this->_value;
}
