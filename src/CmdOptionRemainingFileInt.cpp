#include "CmdOptionRemainingFileInt.hpp"

CmdOptionRemainingFileInt::CmdOptionRemainingFileInt(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingFileInt;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingFileInt::CmdOptionRemainingFileInt(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingFileInt;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingFileInt::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	try {
		this->_value.push_back(std::stoi(str));
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		}
	}

	return error;
}

std::vector<int>		CmdOptionRemainingFileInt::value() const
{
	return this->_value;
}