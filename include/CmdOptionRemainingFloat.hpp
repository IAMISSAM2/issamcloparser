#ifndef CMDOPTIONREMAININGFLOAT_HPP_
#define CMDOPTIONREMAININGFLOAT_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionRemainingFloat : public CmdOption
	{
		public:
					CmdOptionRemainingFloat(std::vector<std::string>);
					CmdOptionRemainingFloat(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<float>		value() const;

		private:
			std::vector<float>		_value;
	};

#endif /* !CMDOPTIONREMAININGFLOAT_HPP_ */
