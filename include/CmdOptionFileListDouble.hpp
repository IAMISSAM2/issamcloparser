#ifndef CMDOPTIONFILELISTDOUBLE_HPP_
#define CMDOPTIONFILELISTDOUBLE_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionFileListDouble : public CmdOption
	{
		public:
					CmdOptionFileListDouble(std::vector<std::string>);
					CmdOptionFileListDouble(std::vector<std::string>, const char*);
					CmdOptionFileListDouble(std::vector<std::string>, bool);
					CmdOptionFileListDouble(std::vector<std::string>, const char*, bool);
			std::string	setValue(const std::string& value);
			std::vector<double>		value() const;

		private:
			std::vector<double>		_value;
			std::string				_separator;
	};

#endif /* !CMDOPTIONFILELISTDOUBLE_HPP_ */
