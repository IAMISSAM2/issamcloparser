#include "CmdOptionRemainingChar.hpp"

CmdOptionRemainingChar::CmdOptionRemainingChar(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::RemainingChar;
	this->_value.clear();
	this->_enabled = false;
}

CmdOptionRemainingChar::CmdOptionRemainingChar(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::RemainingChar;
	this->_value.clear();
	this->_enabled = false;
}

std::string	CmdOptionRemainingChar::setValue(const std::string& value)
{
	std::string error = "";
	if (value.size() == 1) {
		this->_value.push_back(value.front());
	} else {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			error += value;
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			error += value;
			error += " »";
		}
	}
	return error;
}

std::vector<char>		CmdOptionRemainingChar::value() const
{
	return this->_value;
}