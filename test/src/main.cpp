#include "IssamCloParser.hpp"
#include <iostream>

int main(int ac, char** av)
{
	CmdOptionRemainingFileListChar optNumbers(OptNames{}, ": \n\t");

	IssamCloParser cloParser(OptionList{&optNumbers}, ac, av);

	if (cloParser.error()) {
		std::cerr << cloParser.what() << std::endl;
		return 1;
	}
	int i = 0;
	for (auto file : optNumbers.value()) {
		std::cout << "Fichier n°" << ++i << ": " << std::endl;
		for (auto nb : file) {
			std::cout << "Key: \'" << nb << "\'" << std::endl;
		}
	}

	return 0;
}