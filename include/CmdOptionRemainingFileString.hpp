#ifndef CMDOPTIONREMAININGFILESTRING_HPP_
#define CMDOPTIONREMAININGFILESTRING_HPP_

	#include "CmdOption.hpp"
	#include <streambuf>
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>


	class CmdOption;
	class CmdOptionRemainingFileString : public CmdOption
	{
		public:
					CmdOptionRemainingFileString(std::vector<std::string>);
					CmdOptionRemainingFileString(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<std::string>		value() const;

		private:
			std::vector<std::string>		_value;
	};

#endif /* !CMDOPTIONREMAININGFILESTRING_HPP_ */
