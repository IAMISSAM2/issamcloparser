#include "CmdOptionFileFloat.hpp"

CmdOptionFileFloat::CmdOptionFileFloat(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->type = CmdOption::Type::FileFloat;
	this->_value = 0;
	this->_enabled = false;
}

CmdOptionFileFloat::CmdOptionFileFloat(std::vector<std::string> names, bool optional)
{
	this->_names = names;
	this->_optionnal = optional;
	this->type = CmdOption::Type::FileFloat;
	this->_value = 0;
	this->_enabled = false;
}

std::string	CmdOptionFileFloat::setValue(const std::string& value)
{
	std::string error = "";
	std::ifstream file { value };

	if (!file) {
		return "Fichier invalide: « " + value + " »";
	}
	std::string str((std::istreambuf_iterator<char>(file)),
					 std::istreambuf_iterator<char>());

	try {
		this->_value = std::stof(str);
	} catch (std::invalid_argument&) {
		if (this->getNames().size() > 0) {
			error += "Valeur incorrecte pour l'option ";
			if (this->getNames().front().size() == 1) {
				error += "-";
			} else {
				error += "--";
			}
			error += this->getNames().front();
			error += ": « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		} else {
			error += "valeur incorrecte: « ";
			for (auto c : str) {
				if (c != '\n' && c != '\r') {
					error += c;
				}
			}
			error += " »";
		}
	}

	return error;
}

float		CmdOptionFileFloat::value() const
{
	return this->_value;
}
