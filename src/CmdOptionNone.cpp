#include "CmdOptionNone.hpp"

CmdOptionNone::CmdOptionNone(std::vector<std::string> names)
{
	this->_names = names;
	this->_optionnal = false;
	this->_enabled = false;
	this->type = CmdOption::Type::None;
}

CmdOptionNone::CmdOptionNone(std::vector<std::string> names, bool optionnal)
{
	this->_names = names;
	this->_optionnal = optionnal;
	this->_enabled = false;
	this->type = CmdOption::Type::None;
}