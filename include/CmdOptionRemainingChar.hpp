#ifndef CMDOPTIONREMAININGCHAR_HPP_
#define CMDOPTIONREMAININGCHAR_HPP_

	#include "CmdOption.hpp"
	#include <iostream>
	#include <vector>
	#include <string>

	class CmdOption;
	class CmdOptionRemainingChar : public CmdOption
	{
		public:
					CmdOptionRemainingChar(std::vector<std::string>);
					CmdOptionRemainingChar(std::vector<std::string>, bool);
			std::string	setValue(const std::string& value);
			std::vector<char>		value() const;

		private:
			std::vector<char>		_value;
	};

#endif /* !CMDOPTIONREMAININGCHAR_HPP_ */
